﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSystem : MonoBehaviour
{
    public Vector2 getMovementInput()
    {
        return new Vector2 (ControlFreak2.CF2Input.GetAxis("Horizontal"), ControlFreak2.CF2Input.GetAxis("Vertical"));
    }
    public Vector2 getMouseDeltaInput()
    {
        return new Vector2(ControlFreak2.CF2Input.GetAxis("Mouse X"), ControlFreak2.CF2Input.GetAxis("Mouse Y"));
    }
    public Vector2 getMousePositionInput()
    {
        return Input.mousePosition;
    }
    public Vector2 getMouseScrollInput()
    {
        return Input.mouseScrollDelta;
    }
    public bool getJumpInput()
    {
        return ControlFreak2.CF2Input.GetButton("Jump");
    }
    public bool getRunInput()
    {
        return ControlFreak2.CF2Input.GetButton("Run");
    }
    public bool getFireInput()
    {
        return ControlFreak2.CF2Input.GetButton("Fire");
    }
    public bool getZoomInput()
    {
        return ControlFreak2.CF2Input.GetButton("Zoom");
    }
    public bool getReloadInput()
    {
        return ControlFreak2.CF2Input.GetButton("Reload");
    }
    public bool getLeanLeftInput()
    {
        return ControlFreak2.CF2Input.GetButton("LeanLeft");
    }
    public bool getLeanRightInput()
    {
        return ControlFreak2.CF2Input.GetButton("LeanRight");
    }
    public bool getCrouchInput()
    {
        return ControlFreak2.CF2Input.GetButton("Crouch");
    }
    public bool getProneInput()
    {
        return ControlFreak2.CF2Input.GetButton("Prone");
    }
    public bool getUseInput()
    {
        return ControlFreak2.CF2Input.GetButton("Use");
    }
    public bool getExamineInput()
    {
        return ControlFreak2.CF2Input.GetButton("Examine");
    }
    public bool getPauseInput()
    {
        return ControlFreak2.CF2Input.GetButton("Pause");
    }
    public bool getInventoryInput()
    {
        return ControlFreak2.CF2Input.GetButton("Inventory");
    }
    public bool getDigitInput(string Button)
    {
        return ControlFreak2.CF2Input.GetButton(Button);
    }
}
